#require 'byebug'
=begin
load 'lib/player.rb'
load 'lib/board.rb'
load 'lib/battleship.rb'

g = BattleshipGame.new(HumanPlayer.new,Board.new)
=end

class BattleshipGame
  def initialize(player, board)
    @player = player
    @board = board
  end

  attr_reader :board, :player

  def attack(pos)
    @board.grid[pos[0]][pos[1]] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play_turn
    attack(@player.get_play)
  end

  def play_game
    welcome

    @board.place_random_ship

    until game_over?
      @player.display(@board)
      play_turn
    end
    @player.display(@board)
    "game over"
  end

  private

  def welcome
    puts "Welcome to battle ship!"
    puts ""
  end

end
