class Board
  @@default_grid = (0..9).reduce([]){|acc, el| acc << Array.new(10,nil)}

  def self.default_grid
    @@default_grid
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  attr_accessor :grid

  def count
    @grid.flatten.count(:s)
  end

  def empty?(c = nil)
    c ? @grid[c[0]][c[1]] == nil : @grid.flatten.compact.size == 0
  end

  def full?
    !@grid.flatten.include?(nil)
  end

  def place_random_ship

    raise if full?

      x = rand_man
      y = rand_man

    while empty?([x,y]) == false

      x = rand_man
      y = rand_man

    end
      self[[x,y]] = :s
  end

  def won?
    !@grid.flatten.include?(:s)
  end

  def []=(idx,sym)
    @grid[idx[0]][idx[1]] = sym
  end

  def [](idx)
    @grid[idx[0]][idx[1]]
  end

  private

  def rand_man
    (0..@grid.length-1).to_a.sample
  end
end
