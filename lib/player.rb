#require byebug

class HumanPlayer
  def initialize; end

  def get_play
    puts "Row: "
    @row = gets.chomp
    puts "Column: "
    @column = gets.chomp
    raise if @row == 'e'
    [@row.to_i,@column.to_i]
  end

  def display(board)
    #Could do better with a grid.
    print "_"; board.grid.each_index{|i| print "|#{i}|"}; puts ""
    board.grid.each_with_index do |row, i|
      print "#{i}"
      row.each do |el|
        if el==nil
          print "| |"
        else
          print "|#{el}|"
        end
      end
      puts ""
    end
   end
end
